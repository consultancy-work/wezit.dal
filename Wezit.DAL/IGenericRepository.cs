﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Wezit.DataModels.General;

namespace Wezit.DAL
{
    public interface IGenericRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> Get(object id);
        Task<T> Get(string searchField = "",
            string searchText = "", int noOfRows = 1, SearchType searchType = SearchType.Numeric,
            string[] relations = null);
        Task<IEnumerable<T>> GetList(string searchField = "",
            string searchText = "", int noOfRows = 100, SearchType searchType = SearchType.Text,
            string[] relations = null);
        Task<FeedbackMessage> Insert(T entity);
        Task<FeedbackMessage> Update(T entity);
        Task<FeedbackMessage> Delete(object id);
    }
}
