﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Wezit.DataModels.General;
using System.Linq.Dynamic.Core;
using Wezit.DAL.Models;

namespace Wezit.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly wezitContext _context;
        private DbSet<T> _entity;

        public GenericRepository(wezitContext context)
        {
            _context = context;
            _entity = context.Set<T>();
        }

        public async Task<T> Get(object id)
        {
            return await _entity.FindAsync(id);
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            var data = await _entity.ToListAsync();
            return data.AsEnumerable();
        }


        //Need to review performance
        public async Task<T> Get(string searchField = "",
            string searchText = "", int noOfRows = 1, SearchType searchType = SearchType.Numeric,
            string[] relations = null)
        {
            List<T> data;
            IQueryable searchQuery;

            if (relations == null)
            {
                searchQuery = _entity;
            }
            else
            {
                searchQuery = await QueryNavigationProperties(_entity, relations);
            }

            if (string.IsNullOrEmpty(searchField))
            {
                data = await searchQuery.Take(noOfRows)
                        .ToDynamicListAsync<T>();
            }
            else
            {
                string filter = searchType switch
                {
                    SearchType.Text => string.Format("{0}.Contains(@0)", searchField),
                    SearchType.Numeric => string.Format("{0} == @0", searchField),
                    SearchType.GreaterThan => string.Format("{0} > @0", searchField),
                    SearchType.GreaterThanOrEqualTo => string.Format("{0} >= @0", searchField),
                    SearchType.LessThan => string.Format("{0} < @0", searchField),
                    SearchType.LessThanOrEqualTo => string.Format("{0} <= @0", searchField),
                    SearchType.NotEqualTo => string.Format("{0} != @0", searchField),
                    _ => throw new ArgumentOutOfRangeException(),
                };
                data = await searchQuery
                        .Where(filter, searchText)
                        .Take(noOfRows)
                        .ToDynamicListAsync<T>();
            }

            return data.FirstOrDefault();        
        }

        //Need to review performance
        public async Task<IEnumerable<T>> GetList(string searchField = "",
            string searchText = "", int noOfRows = 100, SearchType searchType = SearchType.Text,
            string[] relations = null)
        {
            List<T> data;
            IQueryable searchQuery;

            if (relations == null)
            {
                searchQuery = _entity;
            }
            else
            {
                searchQuery = await QueryNavigationProperties(_entity, relations);
            }

            if (string.IsNullOrEmpty(searchField))
            {
                data = await searchQuery.Take(noOfRows)
                        .ToDynamicListAsync<T>();
            }
            else
            {
                string filter = searchType switch
                {
                    SearchType.Text => string.Format("{0}.Contains(@0)", searchField),
                    SearchType.Numeric => string.Format("{0} == @0", searchField),
                    SearchType.GreaterThan => string.Format("{0} > @0", searchField),
                    SearchType.GreaterThanOrEqualTo => string.Format("{0} >= @0", searchField),
                    SearchType.LessThan => string.Format("{0} < @0", searchField),
                    SearchType.LessThanOrEqualTo => string.Format("{0} <= @0", searchField),
                    SearchType.NotEqualTo => string.Format("{0} != @0", searchField),
                    _ => throw new ArgumentOutOfRangeException(),
                };

                data = await searchQuery
                        .Where(filter, searchText)
                        .Take(noOfRows)
                        .ToDynamicListAsync<T>();
            }

            return data.AsEnumerable();
        }

        private async Task<IQueryable> QueryNavigationProperties(DbSet<T> trackedEntity, string[] relations)
        {
            var finalentityState = Enumerable.Empty<T>();
            foreach (var relation in relations)
            {
                var currentEntityState = await trackedEntity.Include(relation)
                                                    .AsEnumerable<T>()
                                                    .ToDynamicListAsync<T>();
                finalentityState = finalentityState.Union(currentEntityState);
            }

            return finalentityState.AsQueryable();
        }

        public async Task<FeedbackMessage> Insert(T entity)
        {
            if (entity == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,

                    Message = "Entity is null"
                };
            }
            var result = await _entity.AddAsync(entity);
            await _context.SaveChangesAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,

                Data = new { entity = result.Entity, state = result.State },

                Message = result.State.ToString()
            };
        }
        public async Task<FeedbackMessage> Update(T entity)
        {
            if (entity == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured  = true,

                    Message = "Entity is null"
                };
            }
            var result = _entity.Update(entity);
            await _context.SaveChangesAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,

                Data = new { entity = result.Entity, state = result.State },

                Message = result.State.ToString()
            };
        }
        public async Task<FeedbackMessage> Delete(object id)
        {
            var row = await _entity.FindAsync(id);
            if (row == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,

                    Message = "Entity was not found"
                };
            }

            var result = _entity.Remove(row);
            await _context.SaveChangesAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,

                Data = new { deletedEntity = result.Entity, state = result.State },

                Message = result.State.ToString()
            };
        }
    }
}
