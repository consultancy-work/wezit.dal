﻿namespace Wezit.DAL
{ 
    public enum SearchType
    {

        Text,

        Numeric,

        GreaterThan,

        GreaterThanOrEqualTo,

        LessThan,

        LessThanOrEqualTo,

        NotEqualTo

    }
}
