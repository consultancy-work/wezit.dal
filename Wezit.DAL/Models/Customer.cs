﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class Customer
    {
        public Customer()
        {
            CustomerServiceProviders = new HashSet<CustomerServiceProvider>();
            TruckBookings = new HashSet<TruckBooking>();
        }

        public int CustomerId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string PostalAddress { get; set; }
        public string PhysicalAddress { get; set; }
        public int CountryId { get; set; }
        public string City { get; set; }
        public bool IsCompany { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegDocPath { get; set; }
        public string CustomerIdPath { get; set; }
        public bool IsVerified { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<CustomerServiceProvider> CustomerServiceProviders { get; set; }
        public virtual ICollection<TruckBooking> TruckBookings { get; set; }
    }
}
