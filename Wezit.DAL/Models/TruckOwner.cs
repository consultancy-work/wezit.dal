﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class TruckOwner
    {
        public TruckOwner()
        {
            TruckCompanySubscriptions = new HashSet<TruckCompanySubscription>();
            TruckOwnerServiceProviders = new HashSet<TruckOwnerServiceProvider>();
            Trucks = new HashSet<Truck>();
        }

        public int TruckCompanyId { get; set; }
        public string CompanyName { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNumber { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string PhysicalAddress { get; set; }
        public string PostalAddress { get; set; }
        public int CountryId { get; set; }
        public string City { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<TruckCompanySubscription> TruckCompanySubscriptions { get; set; }
        public virtual ICollection<TruckOwnerServiceProvider> TruckOwnerServiceProviders { get; set; }
        public virtual ICollection<Truck> Trucks { get; set; }
    }
}
