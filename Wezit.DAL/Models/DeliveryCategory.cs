﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class DeliveryCategory
    {
        public DeliveryCategory()
        {
            TruckBookings = new HashSet<TruckBooking>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public decimal PricePerUnit { get; set; }

        public virtual ICollection<TruckBooking> TruckBookings { get; set; }
    }
}
