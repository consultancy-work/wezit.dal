﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class CustomerServiceProvider
    {
        public int CustomerProviderId { get; set; }
        public int CustomerId { get; set; }
        public int ServiceProviderId { get; set; }
        public int BookingId { get; set; }

        public virtual TruckBooking Booking { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ServiceProvider ServiceProvider { get; set; }
    }
}
