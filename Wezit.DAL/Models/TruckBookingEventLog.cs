﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class TruckBookingEventLog
    {
        public int EventId { get; set; }
        public int BookingId { get; set; }
        public int EventStageId { get; set; }

        public virtual TruckBooking Booking { get; set; }
        public virtual EventLogStage EventStage { get; set; }
    }
}
