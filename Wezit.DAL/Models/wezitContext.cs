﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class wezitContext : DbContext
    {
        public wezitContext()
        {
        }

        public wezitContext(DbContextOptions<wezitContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BandSubscription> BandSubscriptions { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerServiceProvider> CustomerServiceProviders { get; set; }
        public virtual DbSet<DeliveryCategory> DeliveryCategories { get; set; }
        public virtual DbSet<EventLogStage> EventLogStages { get; set; }
        public virtual DbSet<ServiceProvider> ServiceProviders { get; set; }
        public virtual DbSet<ServiceProviderType> ServiceProviderTypes { get; set; }
        public virtual DbSet<Truck> Trucks { get; set; }
        public virtual DbSet<TruckBooking> TruckBookings { get; set; }
        public virtual DbSet<TruckBookingEventLog> TruckBookingEventLogs { get; set; }
        public virtual DbSet<TruckCompanySubscription> TruckCompanySubscriptions { get; set; }
        public virtual DbSet<TruckOwner> TruckOwners { get; set; }
        public virtual DbSet<TruckOwnerServiceProvider> TruckOwnerServiceProviders { get; set; }
        public virtual DbSet<TruckType> TruckTypes { get; set; }
        public virtual DbSet<TrucksSubscribed> TrucksSubscribeds { get; set; }
        public virtual DbSet<VerificationCode> VerificationCodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Server=localhost; Port=5432; Database=wezit;User Id=postgres; Password=admin100%; Pooling=true; CommandTimeout=60; Timeout=60;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "English_South Africa.1252");

            modelBuilder.Entity<BandSubscription>(entity =>
            {
                entity.HasKey(e => e.BandId)
                    .HasName("band_subscriptions_pk");

                entity.ToTable("band_subscriptions");

                entity.Property(e => e.BandId).HasColumnName("band_id");

                entity.Property(e => e.BandCap).HasColumnName("band_cap");

                entity.Property(e => e.BandName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("band_name");

                entity.Property(e => e.BandRange)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("band_range");

                entity.Property(e => e.Cost)
                    .HasPrecision(12, 2)
                    .HasColumnName("cost");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("countries");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasDefaultValueSql("nextval('country_id_seq'::regclass)");

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("country_name");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("customers");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("city")
                    .HasDefaultValueSql("'Lilongwe'::character varying");

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(90)
                    .HasColumnName("company_name");

                entity.Property(e => e.CompanyRegDocPath)
                    .HasMaxLength(200)
                    .HasColumnName("company_reg_doc_path");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.CustomerIdPath)
                    .HasMaxLength(200)
                    .HasColumnName("customer_id_path");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("email_address");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("firstname");

                entity.Property(e => e.IsCompany).HasColumnName("is_company");

                entity.Property(e => e.IsVerified).HasColumnName("is_verified");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("lastname");

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("phone_number");

                entity.Property(e => e.PhysicalAddress)
                    .IsRequired()
                    .HasColumnName("physical_address");

                entity.Property(e => e.PostalAddress)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("postal_address");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("country_id_fk");
            });

            modelBuilder.Entity<CustomerServiceProvider>(entity =>
            {
                entity.HasKey(e => e.CustomerProviderId)
                    .HasName("customer_provider_pkey");

                entity.ToTable("customer_service_providers");

                entity.Property(e => e.CustomerProviderId)
                    .HasColumnName("customer_provider_id")
                    .HasDefaultValueSql("nextval('customer_provider_id_seq'::regclass)");

                entity.Property(e => e.BookingId).HasColumnName("booking_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.ServiceProviderId).HasColumnName("service_provider_id");

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.CustomerServiceProviders)
                    .HasForeignKey(d => d.BookingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("booking_id_fkey");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerServiceProviders)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_id_fkey");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.CustomerServiceProviders)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("service_provider_id_fkey");
            });

            modelBuilder.Entity<DeliveryCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId)
                    .HasName("delivery_categories_pkey");

                entity.ToTable("delivery_categories");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .HasDefaultValueSql("nextval('category_id_seq'::regclass)");

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("category_name");

                entity.Property(e => e.PricePerUnit)
                    .HasPrecision(18, 2)
                    .HasColumnName("price_per_unit");
            });

            modelBuilder.Entity<EventLogStage>(entity =>
            {
                entity.HasKey(e => e.StageId)
                    .HasName("event_log_stages_pk");

                entity.ToTable("event_log_stages");

                entity.Property(e => e.StageId).HasColumnName("stage_id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");
            });

            modelBuilder.Entity<ServiceProvider>(entity =>
            {
                entity.ToTable("service_providers");

                entity.Property(e => e.ServiceProviderId)
                    .HasColumnName("service_provider_id")
                    .HasDefaultValueSql("nextval('service_provider_id_seq'::regclass)");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(120)
                    .HasColumnName("email_address");

                entity.Property(e => e.Latitude)
                    .HasMaxLength(10)
                    .HasColumnName("latitude");

                entity.Property(e => e.Longitude)
                    .HasMaxLength(10)
                    .HasColumnName("longitude");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(20)
                    .HasColumnName("phone_number");

                entity.Property(e => e.ServiceProviderName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("service_provider_name");

                entity.Property(e => e.ServiceProviderTypeCode)
                    .IsRequired()
                    .HasMaxLength(4)
                    .HasColumnName("service_provider_type_code");

                entity.HasOne(d => d.ServiceProviderTypeCodeNavigation)
                    .WithMany(p => p.ServiceProviders)
                    .HasForeignKey(d => d.ServiceProviderTypeCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("service_provider_type_fkey");
            });

            modelBuilder.Entity<ServiceProviderType>(entity =>
            {
                entity.HasKey(e => e.ProviderTypeCode)
                    .HasName("service_provider_types_pkey");

                entity.ToTable("service_provider_types");

                entity.Property(e => e.ProviderTypeCode)
                    .HasColumnType("character varying")
                    .HasColumnName("provider_type_code");

                entity.Property(e => e.ProviderTypeName)
                    .IsRequired()
                    .HasColumnType("character varying")
                    .HasColumnName("provider_type_name");
            });

            modelBuilder.Entity<Truck>(entity =>
            {
                entity.ToTable("trucks");

                entity.Property(e => e.TruckId).HasColumnName("truck_id");

                entity.Property(e => e.Capacity).HasColumnName("capacity");

                entity.Property(e => e.CurrentLocationId).HasColumnName("current_location_id");

                entity.Property(e => e.RegistrationNumber)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("registration_number");

                entity.Property(e => e.TruckCompanyId).HasColumnName("truck_company_id");

                entity.Property(e => e.TruckImeiNumber)
                    .HasMaxLength(20)
                    .HasColumnName("truck_imei_number");

                entity.Property(e => e.TruckTypeId)
                    .HasColumnName("truck_type_id")
                    .HasDefaultValueSql("1");

                entity.HasOne(d => d.TruckCompany)
                    .WithMany(p => p.Trucks)
                    .HasForeignKey(d => d.TruckCompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("trucks_fk0");

                entity.HasOne(d => d.TruckType)
                    .WithMany(p => p.Trucks)
                    .HasForeignKey(d => d.TruckTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_type_fkey");
            });

            modelBuilder.Entity<TruckBooking>(entity =>
            {
                entity.ToTable("truck_bookings");

                entity.Property(e => e.TruckBookingId).HasColumnName("truck_booking_id");

                entity.Property(e => e.CurrentLocationId).HasColumnName("current_location_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.DestinationLocation)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("destination_location")
                    .HasDefaultValueSql("'N/A'::character varying");

                entity.Property(e => e.LastUpdated).HasColumnName("last_updated");

                entity.Property(e => e.LoadCapacity)
                    .HasPrecision(18, 2)
                    .HasColumnName("load_capacity");

                entity.Property(e => e.LoadCategoryId)
                    .HasColumnName("load_category_id")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.LoadDescription)
                    .IsRequired()
                    .HasColumnName("load_description")
                    .HasDefaultValueSql("'N/A'::text");

                entity.Property(e => e.MotorClinicProviderServiceId).HasColumnName("motor_clinic_provider_service_id");

                entity.Property(e => e.SourceLocation)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("source_location")
                    .HasDefaultValueSql("'N/A'::character varying");

                entity.Property(e => e.TruckId).HasColumnName("truck_id");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TruckBookings)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_bookings_fk1");

                entity.HasOne(d => d.LoadCategory)
                    .WithMany(p => p.TruckBookings)
                    .HasForeignKey(d => d.LoadCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("load_category_id_fkey");

                entity.HasOne(d => d.MotorClinicProviderService)
                    .WithMany(p => p.TruckBookings)
                    .HasForeignKey(d => d.MotorClinicProviderServiceId)
                    .HasConstraintName("motor_clinic_service_fkey");

                entity.HasOne(d => d.Truck)
                    .WithMany(p => p.TruckBookings)
                    .HasForeignKey(d => d.TruckId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_bookings_fk0");
            });

            modelBuilder.Entity<TruckBookingEventLog>(entity =>
            {
                entity.HasKey(e => e.EventId)
                    .HasName("truck_booking_event_log_pk");

                entity.ToTable("truck_booking_event_log");

                entity.Property(e => e.EventId).HasColumnName("event_id");

                entity.Property(e => e.BookingId).HasColumnName("booking_id");

                entity.Property(e => e.EventStageId).HasColumnName("event_stage_id");

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.TruckBookingEventLogs)
                    .HasForeignKey(d => d.BookingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_booking_event_log_fk0");

                entity.HasOne(d => d.EventStage)
                    .WithMany(p => p.TruckBookingEventLogs)
                    .HasForeignKey(d => d.EventStageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_booking_event_log_fk1");
            });

            modelBuilder.Entity<TruckCompanySubscription>(entity =>
            {
                entity.HasKey(e => e.TruckSubscriptionId)
                    .HasName("truck_company_subscriptions_pk");

                entity.ToTable("truck_company_subscriptions");

                entity.Property(e => e.TruckSubscriptionId).HasColumnName("truck_subscription_id");

                entity.Property(e => e.BandId).HasColumnName("band_id");

                entity.Property(e => e.SubscriptionEndDate).HasColumnName("subscription_end_date");

                entity.Property(e => e.SubscriptionStartDate).HasColumnName("subscription_start_date");

                entity.Property(e => e.TruckCompanyId).HasColumnName("truck_company_id");

                entity.HasOne(d => d.Band)
                    .WithMany(p => p.TruckCompanySubscriptions)
                    .HasForeignKey(d => d.BandId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_company_subscriptions_fk1");

                entity.HasOne(d => d.TruckCompany)
                    .WithMany(p => p.TruckCompanySubscriptions)
                    .HasForeignKey(d => d.TruckCompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_company_subscriptions_fk0");
            });

            modelBuilder.Entity<TruckOwner>(entity =>
            {
                entity.HasKey(e => e.TruckCompanyId)
                    .HasName("truck_owners_pk");

                entity.ToTable("truck_owners");

                entity.Property(e => e.TruckCompanyId).HasColumnName("truck_company_id");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("city")
                    .HasDefaultValueSql("'Lilongwe'::character varying");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("company_name");

                entity.Property(e => e.ContactNumber)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("contact_number");

                entity.Property(e => e.ContactPersonFirstName)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("contact_person_first_name");

                entity.Property(e => e.ContactPersonLastName)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("contact_person_last_name");

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("email_address");

                entity.Property(e => e.PhysicalAddress).HasColumnName("physical_address");

                entity.Property(e => e.PostalAddress)
                    .HasMaxLength(50)
                    .HasColumnName("postal_address");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.TruckOwners)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("country_id_fk");
            });

            modelBuilder.Entity<TruckOwnerServiceProvider>(entity =>
            {
                entity.HasKey(e => e.TruckOwnerProviderId)
                    .HasName("truck_owner_provider_pkey");

                entity.ToTable("truck_owner_service_providers");

                entity.Property(e => e.TruckOwnerProviderId)
                    .HasColumnName("truck_owner_provider_id")
                    .HasDefaultValueSql("nextval('truck_owner_provider_id_seq'::regclass)");

                entity.Property(e => e.ServiceProviderId).HasColumnName("service_provider_id");

                entity.Property(e => e.TruckOwnerId).HasColumnName("truck_owner_id");

                entity.HasOne(d => d.ServiceProvider)
                    .WithMany(p => p.TruckOwnerServiceProviders)
                    .HasForeignKey(d => d.ServiceProviderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("service_provider_id_fkey");

                entity.HasOne(d => d.TruckOwner)
                    .WithMany(p => p.TruckOwnerServiceProviders)
                    .HasForeignKey(d => d.TruckOwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_owner_id_fkey");
            });

            modelBuilder.Entity<TruckType>(entity =>
            {
                entity.ToTable("truck_types");

                entity.Property(e => e.TruckTypeId)
                    .HasColumnName("truck_type_id")
                    .HasDefaultValueSql("nextval('truck_type_id_seq'::regclass)");

                entity.Property(e => e.TruckTypeName)
                    .IsRequired()
                    .HasMaxLength(80)
                    .HasColumnName("truck_type_name");
            });

            modelBuilder.Entity<TrucksSubscribed>(entity =>
            {
                entity.HasKey(e => e.SubscriptionId)
                    .HasName("trucks_subscribed_pk");

                entity.ToTable("trucks_subscribed");

                entity.Property(e => e.SubscriptionId).HasColumnName("subscription_id");

                entity.Property(e => e.Approved).HasColumnName("approved");

                entity.Property(e => e.TruckId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("truck_id");

                entity.Property(e => e.TruckSubscriptionId).HasColumnName("truck_subscription_id");

                entity.HasOne(d => d.Truck)
                    .WithMany(p => p.TrucksSubscribeds)
                    .HasForeignKey(d => d.TruckId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("trucks_subscribed_fk0");

                entity.HasOne(d => d.TruckSubscription)
                    .WithMany(p => p.TrucksSubscribeds)
                    .HasForeignKey(d => d.TruckSubscriptionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("truck_subscription_id_fkey");
            });

            modelBuilder.Entity<VerificationCode>(entity =>
            {
                entity.HasKey(e => e.CodeId)
                    .HasName("verification_codes_pkey");

                entity.ToTable("verification_codes");

                entity.Property(e => e.CodeId)
                    .HasColumnName("code_id")
                    .HasDefaultValueSql("nextval('code_id_seq'::regclass)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnType("character varying")
                    .HasColumnName("code");

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(60)
                    .HasColumnName("phone_number");
            });

            modelBuilder.HasSequence("category_id_seq").HasMax(999999999999);

            modelBuilder.HasSequence("code_id_seq").HasMax(999999999999);

            modelBuilder.HasSequence("country_id_seq").HasMax(999999999999);

            modelBuilder.HasSequence("customer_provider_id_seq").HasMax(999999999999);

            modelBuilder.HasSequence("service_provider_id_seq").HasMax(999999999999);

            modelBuilder.HasSequence("truck_owner_provider_id_seq").HasMax(999999999999);

            modelBuilder.HasSequence("truck_type_id_seq").HasMax(999999999999);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
