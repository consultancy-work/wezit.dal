﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class ServiceProvider
    {
        public ServiceProvider()
        {
            CustomerServiceProviders = new HashSet<CustomerServiceProvider>();
            TruckBookings = new HashSet<TruckBooking>();
            TruckOwnerServiceProviders = new HashSet<TruckOwnerServiceProvider>();
        }

        public int ServiceProviderId { get; set; }
        public string ServiceProviderTypeCode { get; set; }
        public string ServiceProviderName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public virtual ServiceProviderType ServiceProviderTypeCodeNavigation { get; set; }
        public virtual ICollection<CustomerServiceProvider> CustomerServiceProviders { get; set; }
        public virtual ICollection<TruckBooking> TruckBookings { get; set; }
        public virtual ICollection<TruckOwnerServiceProvider> TruckOwnerServiceProviders { get; set; }
    }
}
