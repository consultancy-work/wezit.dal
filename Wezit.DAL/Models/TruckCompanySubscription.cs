﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class TruckCompanySubscription
    {
        public TruckCompanySubscription()
        {
            TrucksSubscribeds = new HashSet<TrucksSubscribed>();
        }

        public int TruckSubscriptionId { get; set; }
        public int TruckCompanyId { get; set; }
        public int BandId { get; set; }
        public DateTime SubscriptionStartDate { get; set; }
        public DateTime SubscriptionEndDate { get; set; }

        public virtual BandSubscription Band { get; set; }
        public virtual TruckOwner TruckCompany { get; set; }
        public virtual ICollection<TrucksSubscribed> TrucksSubscribeds { get; set; }
    }
}
