﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class BandSubscription
    {
        public BandSubscription()
        {
            TruckCompanySubscriptions = new HashSet<TruckCompanySubscription>();
        }

        public int BandId { get; set; }
        public string BandName { get; set; }
        public string BandRange { get; set; }
        public int BandCap { get; set; }
        public decimal Cost { get; set; }

        public virtual ICollection<TruckCompanySubscription> TruckCompanySubscriptions { get; set; }
    }
}
