﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class TrucksSubscribed
    {
        public int SubscriptionId { get; set; }
        public int TruckId { get; set; }
        public bool Approved { get; set; }
        public int TruckSubscriptionId { get; set; }

        public virtual Truck Truck { get; set; }
        public virtual TruckCompanySubscription TruckSubscription { get; set; }
    }
}
