﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class TruckBooking
    {
        public TruckBooking()
        {
            CustomerServiceProviders = new HashSet<CustomerServiceProvider>();
            TruckBookingEventLogs = new HashSet<TruckBookingEventLog>();
        }

        public int TruckBookingId { get; set; }
        public int TruckId { get; set; }
        public int CustomerId { get; set; }
        public int? CurrentLocationId { get; set; }
        public DateTime LastUpdated { get; set; }
        public string LoadDescription { get; set; }
        public int LoadCategoryId { get; set; }
        public decimal LoadCapacity { get; set; }
        public string SourceLocation { get; set; }
        public string DestinationLocation { get; set; }
        public int? MotorClinicProviderServiceId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual DeliveryCategory LoadCategory { get; set; }
        public virtual ServiceProvider MotorClinicProviderService { get; set; }
        public virtual Truck Truck { get; set; }
        public virtual ICollection<CustomerServiceProvider> CustomerServiceProviders { get; set; }
        public virtual ICollection<TruckBookingEventLog> TruckBookingEventLogs { get; set; }
    }
}
