﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class VerificationCode
    {
        public long CodeId { get; set; }
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
    }
}
