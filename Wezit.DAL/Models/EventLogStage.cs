﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class EventLogStage
    {
        public EventLogStage()
        {
            TruckBookingEventLogs = new HashSet<TruckBookingEventLog>();
        }

        public int StageId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<TruckBookingEventLog> TruckBookingEventLogs { get; set; }
    }
}
