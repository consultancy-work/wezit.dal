﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class ServiceProviderType
    {
        public ServiceProviderType()
        {
            ServiceProviders = new HashSet<ServiceProvider>();
        }

        public string ProviderTypeCode { get; set; }
        public string ProviderTypeName { get; set; }

        public virtual ICollection<ServiceProvider> ServiceProviders { get; set; }
    }
}
