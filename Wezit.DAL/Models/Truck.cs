﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class Truck
    {
        public Truck()
        {
            TruckBookings = new HashSet<TruckBooking>();
            TrucksSubscribeds = new HashSet<TrucksSubscribed>();
        }

        public int TruckId { get; set; }
        public int TruckCompanyId { get; set; }
        public string RegistrationNumber { get; set; }
        public int Capacity { get; set; }
        public int CurrentLocationId { get; set; }
        public int TruckTypeId { get; set; }
        public string TruckImeiNumber { get; set; }

        public virtual TruckOwner TruckCompany { get; set; }
        public virtual TruckType TruckType { get; set; }
        public virtual ICollection<TruckBooking> TruckBookings { get; set; }
        public virtual ICollection<TrucksSubscribed> TrucksSubscribeds { get; set; }
    }
}
