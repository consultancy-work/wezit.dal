﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class Country
    {
        public Country()
        {
            Customers = new HashSet<Customer>();
            TruckOwners = new HashSet<TruckOwner>();
        }

        public int CountryId { get; set; }
        public string CountryName { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<TruckOwner> TruckOwners { get; set; }
    }
}
