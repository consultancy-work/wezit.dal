﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class TruckOwnerServiceProvider
    {
        public int TruckOwnerProviderId { get; set; }
        public int TruckOwnerId { get; set; }
        public int ServiceProviderId { get; set; }

        public virtual ServiceProvider ServiceProvider { get; set; }
        public virtual TruckOwner TruckOwner { get; set; }
    }
}
