﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Wezit.DAL.Models
{
    public partial class TruckType
    {
        public TruckType()
        {
            Trucks = new HashSet<Truck>();
        }

        public int TruckTypeId { get; set; }
        public string TruckTypeName { get; set; }

        public virtual ICollection<Truck> Trucks { get; set; }
    }
}
